"use strict";

// const product = {
//   name: "Mobile",
//   price: 5000,
//   discount: 20,
// };

// product.getPrice = function () {
//   let result = this.price - this.price * (this.discount / 100);
//   return result;
// };

// console.log(product.getPrice());

// function userNameAge() {
//   let name = prompt("Введіть своє ім'я:");
//   let age = prompt("Введіть свій вік:");
//   const user = {
//     name: name,
//     age: age,
//   };

//   return `Привіт мене звуть ${name} та мені ${age} років!`;
// }

// alert(userNameAge());

// const product = {
//   name: "Mobile",
//   price: 5000,
//   model: 10,
// };
// function cloneFn(obj) {
//   let clone;
//   for (let key in obj) {
//     if (obj.hasOwnProperty(key)) {
//       if (typeof obj[key] === "object" && obj[key] !== null) {
//         clone[key] = deepClone(obj[key]);
//       } else {
//         clone[key] = obj[key];
//       }
//     }
//   }
//   return clone;
// }
// const clonedProduct = cloneFn(product);

// console.log(clonedProduct);

const product = {
  name: "Mobile",
  price: 5000,
  model: 10,
};

function cloneFn(obj) {
  if (typeof obj !== "object") {
    return obj;
  }

  const clone = Array.isArray(obj) ? [] : {};

  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      if (typeof obj[key] === "object" && obj[key] !== null) {
        clone[key] = cloneFn(obj[key]);
      } else {
        clone[key] = obj[key];
      }
    }
  }

  return clone;
}

const cloneProduct = cloneFn(product);

console.log(cloneProduct);
